CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

A experience field type to save the user experience by year with month
select list and display like 02 Year(s) 07 Month(s).

This experience field type is possible to use in job portals,application
forms (capture the total year of experience, relevant field experience )
and user registration.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/experience

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/experience


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.
